---
title: Ciberplanillerxs
description: 'Una reflexión colectiva sobre la recolección de datos en contexto de urgencia'
author:
- Partido Interdimensional Pirata
categories:
- Argentina
layout: post
---

A pocos días de aparecide le "paciente 0" en territorio del estado
argentino, comenzó a circular por medio de organizaciones sociales la
promesa de un subsidio temporal que finalmente nunca se anunció ni
tampoco puso en marcha.

El viernes a las 00.00 comenzaba la cuarentena y las orgas se enteraban
que iban a tener que moverse rapidísimo para completar unas planillas de
subsidio a la economía popular. Los items para llenar, como es de
esperar, requerían completar con datos legales muy precisos de cada
persona que podía recibir el subsidio según las condiciones que este
imponía.

Desde el PIP siempre promovemos ([entre otras
cosas](https://utopia.partidopirata.com.ar/quiero_ser_pirata.html#qu%C3%A9-hacemos))
la autodefensa digital, que en ciertas prácticas adopta concretamente la
forma de una defensa de la privacidad.

Y esto nos lleva a una primera _reacción_ en tanto activistas por la
autodefensa digital, porque ¿por qué hay que recolectar información
totalmente sensible?  ¿Por qué se estaba recolectando a través de
planillas de Google? Etc.

Pero así como en su momento nos preguntamos por qué nosotres
defenderíamos la privacidad como un bien común, cuando ésta suele estar
tan ligada al [concepto de propiedad
privada](https://es.wikipedia.org/wiki/Propiedad_privada) (al cual no le
tenemos simpatía); ahora nos preguntamos: ¿A quién queremos llegar
cuando indicamos que no hay que subir datos sensibles a planillas de
google o excels que circulan sin una firma oficial del gobierno?
(¿Importa si era con firma oficial?)

Pero la reflexión puede bucear en la realidad y revelar que no se tratá
de si le damos o no nuestro nombre y/ o los detalles mas íntimos acerca
del universo de posibilidades que conforman la realidad de no habitar
una vivienda digna.

El anonimato entonces aparece como una estrategia que sólo es posible
desde un montón de privilegios. Así, resguardar nuestros datos de las
empresas y gobiernos no es una tarea posible en su totalidad. Querer
llevar a cabo una vida anónima, removida del control cibernético,
requeriría un trabajo y una astucia que no nos permitiría llevar nuestra
vida cotidiana... y muy probablemente, atentaría contra nuestra salud
mental y nuestros medios de subsistencia. La praxis requiere de una
construcción codo a codo. Su forma más inmediata es la de una política
de cuidados mutuos, que pueda decidir caso por caso y cómo y por dónde
se recolectan los datos, para no exponer a nadie y lograr nuestros
objetivos colectivos.

Debemos agregar que todo esto estaba sucediendo sin acceso directo
e individual a las planillas, a través de redes que solo tendrían la
capacidad de armar las organizaciones de base y a pesar de las
conexiones carísimas y desiguales que hay en los barrios.

Entonces, ¿el anonimato es un privilegio o es en verdad un privilegio
poder tener acceso a ciertos recursos mediante la inscripción con
nuestros datos personales? Ninguna de esas opciones nos da una respuesta
satisfactoria. Lo que sucede al cargar datos en sitios que no conocemos
o en sitios que conocemos demasiado bien como Google es que no sabemos
cuáles serán los usos derivados de esa recolección datos. Por eso no
podemos ser puristas del anonimato o del resguardo de datos sensibles en
un mundo donde las grandes empresas de comunicación son quienes manejan
los medios masivos de información, imponiendo sus herramientas como las
hegemónicas. Intentaremos acercar información teniendo en cuenta que las
estrategías para cada organización son singulares y demostrando cómo
podemos construir alternativas que pueden ser mucho más cuidadosas
y compañeras con nuestros objetivos.
