---
title: Ciberyuta del humor
description: El ciberpatrullaje como dispositivo de disciplinamiento
author:
- V
- Con colaboración de LucioRtg
categories:
- Balcarce, Argentina
layout: post
---

"Al Presidente no le gustó tu tuit"[^cy1]. Eso dijo la DDI a KG cuando
lo fueron a buscar a su casa para informarle que se encontraba bajo una
causa penal que lo acusa de **intimidación** e **instigación a cometer
un delito**. El tuit era un chiste. Una alegoría a un meme que viene
circulando hace tiempo, y que refiere a la posibilidad de emprender
s@k30s cuando la situación del país sea crítica.

La advertencia de la Ministra Frederic y  su séquito ciberyuta acerca de
la vigilancia de nuestro humor era un aviso literal.

El fiscal que le abrió la causa, Rodolfo Morue,  expresó al respecto:
"En un grupo social donde participan 700 personas, entendimos que **era
una amenaza seria** como para notificarlo de sus derechos y avisar que
nosotros sabíamos que **podría tener un plan** en su cabeza y que debía
desistir. Si es un chiste o ironía solo lo sabe él. **No estamos
viviendo una situación como para hacer chistes en redes sociales**",
continuó el fiscal[^cy2].

El Estado ahora detenta la potestad no solo de detectar el humor social,
también de moldearlo, disciplinarlo. Y parece que éste chiste no le
resultó gracioso. Es la primera acusación judicial relacionada al
ciberpatrullaje desde que se anunció el 7 de abril su puesta en marcha.
El ciberpatrullaje es una modalidad de la ciberseguridad, a través de la
vigilancia con cruzamiento de palabras clave **permite identificar la
posible comisión de delitos**. _Implica el monitoreo de contenido como
mensajes, posteos, imágenes y todo lo que se publica en alguna red
social. Se realizan actividades de recolección, almacenamiento
y análisis de los datos recolectados para transformarlos en información
que sea de utilidad a la causa_[^cy3].

"Todas las fuerzas realizan 'ciberpatrullaje' en las redes sociales para
detectar el humor social, dependiendo de la zona y trabajar en alertas
tempranas para prevenir diversas situaciones", dijo Frederic en una
conferencia, y agregó que "Las razones por las que sirve el
ciberpatrullaje es porque **la gente está mucho en la casa y a veces
incita a la violencia** [...]"[^cy4].

Entre las características más criticables de esta práctica podemos
resaltar que:

* Representa un atropello contra la libertad de expresión y un
  condicionamiento del discurso público. Tras este acontecimiento es muy
  probable que se sosieguen opiniones políticas críticas en las redes
  debido al temor latente a una vigilancia incesante.

* Vulnera la privacidad de les usuaries, _ya que por más de que se estén
  monitoreando sus expresiones en la esfera pública, como pueden ser las
  redes sociales, **tienen una expectativa de privacidad y no esperan
  que se haga un seguimiento de sus expresiones, recolección
  y tratamiento de su información**. Estas constituyen tareas de
  inteligencia_.

* Los programas que se utilizan para recoger palabras o frases no tienen
  capacidad de identificar el contexto ni la razón por las que las
  personas las usan.

* Carece de un protocolo que regule su accionar --actualmente está en
  elaboración, sin embargo la ciberyuta se encuentra operando-- y en
  algunas Provincias se delega esta práctica a unidades que están
  exentas de cualquier tipo de control[^cy5]. Sin embargo entendemos que
  un "control" extra-estatal tampoco sería una solución a este
  problema. La vigilancia MASIVA sin consentimiento de les usuaries es
  ilegítima ya sea bajo normas que la regulen o sin ellas.


## La causa

Los efectos --de poder-- de la causa judicial que le abrieron a KG, al
igual que los del aparato judicial en su conjunto, pueden ser mejor
entendidos en su positividad antes que en su negativdad. Con esto me
refiero a que la causa **no es** un castigo,no es un efecto punitivo en
sí --lo que representaría un efecto de poder **negativo**. Es en cambio
la amenaza de uno. No juega un papel coactivo o represivo.

En realidad ésta acusación y su difusión mediática masiva juegan un
efecto de poder positivo en el disciplinamiento de les cuerpes, en la
producción de opiniones, conductas y deseos. La positividad del poder
deviene de la facilitación del disciplinamiento, de los medios que
moldean la ideología, de la suscitación al deseo a ser gobernade. Los
efectos de poder positivos generan deseos reaccionarios, fascistas. No
precisa reprimir. El ciberpatrullaje es, por antonomasia, el instrumento
que facilita un mayor acceso a la información personal y posibilita la
retroalimentación de este sistema disciplinario.

Estos efectos positivos de poder se evidencian a partir de estos
3 fenómenos:

1. El objetivo REAL de la ciberyuta es capturar los deseos subversivos
   y recodificarlos. A partir de ahí se procederá canalizando esa
   energía para encauzarla hacia otra parte y así conjurar el
   acontecimiento. Con esto quiero decir que va proceder identificado
   a la **pulsión** subversiva antes que a la **amenaza** criminal como
   supuestamente pretende.

   Esta recodificación del deseo apela a la docilidad y mansedumbre de
   les cuerpes. Esperan que cuando la situación económica sea
   insostenible --cuando el hambre retuerza las tripas y DUELA, cuando
   ya no alcance la guita que papi Estado reparte frenético, cuando los
   milicos ya solo distribuyan migajas y miedo en las villas-- puedan
   apelar a la buena conciencia de la gente. A una conciencia libre de
   deseos anómalos. Buscan que no se nos cruce por la cabeza la
   posibilidad de llevar a cabo s@k30s --de hecho esto fue expresado por
   Frederic[^cy6] --difícil tarea, ya que el deseo no fue totalmente
   desterrado --desterritorializado-- y tiene su nido en el inconciente
   colectivo nacional. Late el recuerdo aún tibio de las insurrecciones
   del 2001 y 2013.

2. El aparato judicial no actúa de manera meramente punitiva, sino que
   establece una acusación: "KG incita a la delincuencia con sus
   chistes" que es difundida masivamente e internalizada por las
   personas. Entonces se expone como aparato antisubversivo
   (foucaultiano) que busca generar una **polarización** en la sociedad
   mediante la identificación de la OTREDAD anómala, insurrecta,
   malandra, piba chorra. A través de un proceso de **criminalización**
   determina a la OTREDAD. Actúa a su vez como un mecanismo
   individualizante, hay un doble proceso: Te identificamos a vos KG,
   examinamos tus gustos, movimientos, deseos; luego te exhibimos como
   dividuo de riesgo, factor potencial del kaos. Para este fin los
   medios de in-comunicación como Clarín y TN no dudaron en hacer
   alusión a una supuesta **causa de robo** en el expediente de KG --que
   es útil en esta formación de la percepción pública de KG como
   delincuente y que además pondría en duda el carácter de "chiste" de
   su tuit[^cy7].

   Mediante la identificación de la OTREDAD efectivamente se vuelve más
   fácil la tarea de disciplinamiento de las conductas ya que cada
   sujeta procede por incorporar los mandatos, deseos,  aversiones
   y comportamientos que pregona el Sistema --es decir que se
   autodisciplina-- y/o tiende a disciplinar a otres. Esto es así porque
   cuando se delimita a la Otredad y se la exhibe, se difunde
   simultáneamente su antítesis (La Otredad no son las sujetas a la
   legalidad, las amantes de la moral) y se incita a un castigo
   moralizador desde el seno de la sociedad.

   KG comunicó que tras haber sido publicada la noticia en los medios
   locales, comenzó a recibir mensajes insultándolo y a ser víctima de
   hostigamiento[^cy8]. La identificación y exclusión de la OTREDAD
   también se evidencia en los abusos de poder protagonizados por las
   fuerzas policiales y gendarmería --que son las que con mayor
   excitación incorporan el discurso de la Otredad-- generalmente hacia
   trabajadorxs populares y personas racializadas.

3. Otro efecto de la Causa es generar la sensación de una vigilancia
   constante e imperceptible; el temor y cierta incomodidad al concebir
   que nuestras publicaciones y comentarios estarán (y lo están) siendo
   filtrados por algoritmos categorizantes y acechadas por la
   ciberchusma de buró. Los mecanismos que se utilizaban para la
   persecución de _grooming_, narcotráfico, entre otras, ahora se
   utilizan para medir el humor social, procesar los deseos y la
   opinión pública. El monitoreo en redes sociales para la prevención de
   crímenes agravados se metamorfoseó en vigilancia masiva --e ilegal--
   de sujetas --de sus opiniones, conductas y deseos.  Será un pequeño
   paso, un comienzo del despliegue del ciberpatrullaje en nuestras
   vidas virtuales; pero es sin lugar a dudas un gran salto para el
   fascismo.


## Reacción

Una de las reacciones suscitadas por la difusión de la causa fue que
parte de la población local comenzó a hostigar a KG mediante insultos
y que se gestara en las consciencia el miedo al s@k30 y al s@k3ad0r. Sin
embargo no es la única reacción. En Twitter la palabra s@k30 fué
tendencia, ya que varies usuaries decidieron publicar el mismo tuit en
protesta contra la persecución ideológica efectuada por la ciberyuta
albertista. Esto demuestra que una resistencia creativa es posible (y es
REAL) a través de la proliferación de los ruidos, de la saturación del
sistema y el boicot (o al menos ralentización) al trabajo de
identificación y categorización de publicaciones de contenido político
contestatario o que escapan a las codificaciones "morales" del sistema.

El ciberpatrullaje es el instrumento magnífico y brutal que utiliza el
Estado para intensificar la aplicación de efectos positivos de poder
sobre la sociedad --los cuales no son novedad, ya estaban siendo
sumistrados desde mucho antes, lo preocupante es que se vuelven cada vez
más abarcativos, totalizadores y fascistas. En este sentido la ciberyuta
recolecta información sobre las conductas, características y deseos de
les cuerpes, con la finalidad de agudizar y perfeccionar la supresión
del deseo subversivo --en este caso del s@k30s--, su recodificación y la
producción de investimientos sociales que sean favorables al Estado (el
deseo a quedarse en casa, por ejemplo). Lo peligroso acá entonces no es
la vigilancia en sí --más allá de que sea realmente nefasta-- sino sus
consecuencias y su potencial utilidad.

Creemos que los fines --evitar más muertes y contagios de coronavirus
manteniendo un escenario social pacífico-- no justifican los medios
--vigilancia masiva ilegal que posibilita un ejercicio de poder
totalitario y busca someter nuestros deseos a recodificaciones, además
de crear subjetivaciones convenientes: la sujeta chusma de balcón,
higiénica, de fiel escucha mediática, adicta al mainstream. Su peor
pesadilla es el pibe que le "chorea" su salud tosiéndole en la cara o no
respetando los 2m de distancia en "el chino".

Denunciamos que estos mecanismos de vigilancia (CIBYUT) están llevando
a cabo una suerte de disciplinamiento moralizador, un atentado contra la
libre expresión y que juega un papel primordial como reproductor de
estigmas. Asimismo incentivamos a la resistencia **creativa** contra
estos atropellos neofascistas. Confiamos en que será esencial que la
improvisación, conjunción de fuerzas, y la imaginación estén de nuestro
lado en la insurrección que viene.


[^cy1]: <http://www.laizquierdadiario.com/Kevin-Guerra-La-DDI-me-dijo-al-Presidente-no-le-gusto-tu-tuit-y-ahora-estoy-procesado>

[^cy2]: <https://tn.com.ar/policiales/ciberpatrullaje-hizo-un-chiste-en-twitter-y-le-abrieron-una-causa-por-intimidacion_1061161>

[^cy3]: <https://www.vialibre.org.ar/2020/04/13/ciberpatrullaje-los-expertos-creen-que-es-una-practica-peligrosa-y-que-pone-en-riesgo-la-libertad-de-expresion/>

[^cy4]: <https://tn.com.ar/tecno/f5/polemica-por-el-ciberpatrullaje-en-las-redes-para-detectar-el-humor-social_1058733>

[^cy5]: <https://www.clarin.com/politica/coronavirus-argentina-opositores-entidades-prensa-piden-limitar-controlar-ciberpatrullaje-fuerzas-seguridad_0_UCb8gBzIh.html>

[^cy6]: <https://www.clarin.com/politica/coronavirus-argentina-sabina-frederic-defendio-ciberpatrullaje-admitio-hablar-humor-social-feliz-_0_sEOYCH5v_.html>

[^cy7]: <https://www.clarin.com/policiales/-sigue-pie-saqueo-tuit-valio-causa-judicial-joven-balcarce_0_Q2GKqAIe1.html>

[^cy8]: <https://tn.com.ar/policiales/ciberpatrullaje-hizo-un-chiste-en-twitter-y-le-abrieron-una-causa-por-intimidacion_1061161>
