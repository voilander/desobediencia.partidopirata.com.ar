---
title: "Convocatoria: Dossier Desobediencia Cibernética"
description: |
  Frente a la aceleración que provoca la cibernética, la velocidad o el
  nomadismo sólo pueden representar elaboraciones secundarias vis-à-vis
  de las políticas de ralentización
author:
- Partido Interdimensional Pirata
categories:
- Convocatoria
layout: post
order: 1000
---

> "No cabe duda de que la conducción, el abandono o la movilidad en
> general son capaces de acrecentar la amplificación de los desvíos con
> respecto a la norma, a condición de reconocer qué es lo que interrumpe
> los flujos en el seno mismo de la circulación. Frente a la aceleración
> que provoca la cibernética, la velocidad o el nomadismo sólo pueden
> representar elaboraciones secundarias vis-à-vis de las políticas de
> ralentización" -- [La hipótesis
> cibernética](http://gen.lib.rus.ec/book/index.php?md5=D48B61549852A4F9255C685292DA85D0),
> Comité Invisible.

Desde la declaración de la pandemia y las consecuentes cuarentenas
declaradas por los estados se han legitimado rápidamente las medidas de
control cibernético.  Estas posibilidades tecnocráticas ya existían,
estaban allí, esperando la oportunidad para ser puestas en marcha. La
más ubicua, el rastreo de movimientos a través teléfonos celulares.

En este dossier las recolectamos, de forma que las reconozcamos, las
señalemos y podamos darnos estrategias colectivas para desmantelar el
estado de control.

La situación de encierro y no-movilización deja al descubierto las pocas
redes tendidas de herramientas que tenemos para organizarnos, pensando
en las pocas redes de contención entre pares, intercambio y subsistencia
que tenemos a nuestro alrededor y la plena dependencia de suministros
básicos basada en trabajo doblemente precarizado.

**Invitamos a que nos acerquen textos y caracterizaciones sobre cómo está
la situación en sus territorios en cuanto al control cibernético en
situación de pandemia y cuarentena. Estilo y longitud libres, para
publicar íntegros junto con los demás.**

## Contactos

Por correo: <contacto@partidopirata.com.ar>

En el Fediverso: <https://todon.nl/@pip>

En Telegram: <https://t.me/ReneMontes_bot>

En Twitter: <https://twitter.com/PartidoPirataAr>

