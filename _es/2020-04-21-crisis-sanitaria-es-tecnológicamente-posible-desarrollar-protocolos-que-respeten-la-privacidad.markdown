---
title: 'Crisis sanitaria: es tecnológicamente posible desarrollar protocolos que respeten la privacidad'
description: |
  La política de los datos sí importa, determina la forma en que
  podremos o no vivir en un espacio más libre en un futuro próximo
author:
- Sursiendo
categories:
- Chiapas, México
layout: post
---

> Publicado en [Sursiendo.org](https://sursiendo.org/blog/2020/04/crisis-sanitaria-es-tecnologicamente-posible-desarrollar-protocolos-que-respeten-la-privacidad/)

La semana pasada nos convocaron a conversar en la **Escuela de
Activistas** de la organización [Ingeniería Sin Fronteras
Euskadi](https://mastodon.eus/@ISFEuskadi). De esa plática sobre
[Vulneración de derechos digitales en un estado de alarma
global](https://sursiendo.org/blog/2020/04/platica-vulneracion-de-derechos-digitales-en-un-estado-de-alarma-global/)
salieron varias reflexiones que trascendían los propios derechos
digitales a la vez de permanecer vinculados a ellos.

Controlar la pandemia ha propuesto como **solución de ¿corto plazo?** la
‘necesidad’ de ceder derechos y libertades, en particular la privacidad,
libertad de expresión y de reunión, a cambio de estar ‘a salvo’.

Los gobiernos de todo el mundo corrieron \[más o menos rápido, según el
caso\] a buscar en la tecnología una aliada que permita ayudar en esa
detención ‘de la pandemia’. **Tal y como están diseñadas esas nuevas
tecnologías lo que están controlando es a su ciudadanía**. Aquí un
[listado \[no exhaustivo\] de las aplicaciones de seguimiento del
mundo](https://fs0c131y.com/covid19-tracker-apps/).

Se está manejando la crisis desde la amenaza. Estar aisladas en casa la
mayor cantidad de tiempo posible es la única medida posible, al
parecer. Estamos en soledad y con miedo. **La narrativa de la crisis
profundiza psicológicamente la idea de que estar con otras personas es
peligroso, el/la vecina como enemiga, apoyando la ruptura del ya
debilitado tejido social en el que estamos sumergidos desde hace
décadas**.

En el [segundo episodio de System
Update](https://www.youtube.com/watch?v=Nd7exbDzU1c&feature=youtu.be),
el periodista **Glenn Greenwald** se centra en las ramificaciones
no-exploradas de las decisiones políticas que tomamos de forma masiva
para responder a la crisis. Brinda un **panorama 360°** sobre la crisis
al tratar el tema de los derechos digitales, las afectaciones sociales,
culturales, políticas y psicológicas de la misma y cómo lo que comemos
[la industrialización de la ganadería y la agricultura extensiva]
influyen en la propagación de epidemias.

Además, resuena una reflexión histórica en nuestras cabezas. Las
acciones tomadas tras el 11S nos demuestran que los poderes
extraordinarios y ‘temporales’ otorgados para ‘controlar’ la crisis no
han dejado de existir casi 20 años después. ¿Vivimos entonces en un
estado permanente de alarma global? Dice Gleenwald, **‘seamos realistas,
reconozcamos que esos poderes, incluso si realmente queremos que sean
temporales es altamente probable que no lo sean’**. Por eso las
decisiones que tomamos sobre cómo afrontar los riesgos de hoy tendrán
repercusiones en el futuro. Lo que aceptemos como normal ahora, puede
ser la norma de ahora en adelante.

En ese sentido, **¿puede la tecnología tener un rol protagónico y de
apoyo al control de expansión de la pandemia respetando derechos
y libertades? Sí puede**. Es una decisión política no hacerlo.

Las tecnologías condensas siglos de conocimiento en técnica, matemática,
ingeniería, física, electrónica, lingüística, relaciones humanas,
laborales, naturales. El brillo de los aparatitos [y las patentes]
invisibilizan el cúmulo de conocimiento allí alojado. Esas relaciones
entre ciencia y política pueden ser desarrolladas de una forma o de
otras. Para la vigilancia y el control \[que es el camino actual que
adoptaron gobiernos y corporaciones\] o para el beneficio social \[que es
el camino propuesto por diversos espacios de sociedad civil\].

¿Cómo es posible crear tecnologías que tomen la privacidad por diseño en
el desarrollo de las tecnologías de seguimiento? Desde el [Caos Computer
Club](https://twitter.com/chaosupdates) compartieron un [decálogo para
evaluar las aplicaciones de
seguimiento](https://www.ccc.de/en/updates/2020/contact-tracing-requirements)
basado en la voluntariedad en la entrega de datos, anonimato de los
mismos, transparencia, descentralización, economía de datos \[extraer
solo los necesarios\]. Mientras tanto, [Xnet compartió un
artículo](https://www.infolibre.es/noticias/opinion/plaza_publica/2020/04/06/datos_ciudadania_instituciones_circulo_virtuoso_para_atacar_covid_19_105660_2003.html)
en el que, además de hacer hincapié en la apertura del código y la
transparencia enfoca en la **posibilidad no explorada por gobiernos de
la cooperación ciudadana y el _big data_ para el bien común**. Ese
último concepto, tan dialogado en las comunidades de derechos digitales,
tiene una oportunidad histórica para ser ejercido. Hacer «un uso de los
datos masivos de la población siempre que sean anonimizados y abiertos
para que no solo gobiernos y corporaciones, sino también la ciudadanía
puedan usarlos, no solo es positivo, sino que es el futuro. El _big
data_ debe estar supeditado a reglas democráticas: debe ser un bien
común al servicio de la ciencia e innovación ciudadanas y no un bien
privativo de gobiernos y corporaciones».

**¿Entonces sí pueden congeniarse salud pública y derechos digitales?**
En las últimas semanas encontramos [comunidades tecnológicas en defensa
de la privacidad y la criptografía trabajando ‘con un espíritu de
colaboración radical’ en protocolos basados en este
principio](https://www.zfnd.org/blog/private-contact-tracing-protocols-compared/):
«es de vital importancia evitar la creación de una nueva infraestructura
de vigilancia que dé más poder a las mismas instituciones cuyos fallos
contribuyeron a la crisis». Los dos protocolos que están posicionándose
respecto al tema son el
[TCN/CEN](https://github.com/TCNCoalition/TCN/blob/main/README.md)
impulsado por la fundación estadunidense Zcash, Co-Epi y Covid-Watch; el
otro es llamado protocolo [DP3-T](https://github.com/DP-3T/documents)
y está siendo trabajado por investigadores de ocho universidades
europeas.

Ambos **protocolos son muy similares en su diseño** y proponen tres
fases para el rastreo del virus: emisión, informe y escaneo. Se propicia
el **rastreo descentralizado** de los contactos mediante transmisiones
por _Bluetooth_ de los dispositivos móviles de los usuarios que no
revelan información sobre la identidad o el historial de ubicación de
las personas usuarias. Luego, si esas personas desarrollan síntomas
o dan positivo en un testeo, pueden **enviar un informe** subiendo un
paquete con ciertos datos a un servidor. Unas terceras personas
**supervisan los datos publicados** por el servidor, quien avisa a los
dispositivos que hayan estado en contacto con ese dispositivo sobre la
situación de salud. Algo así como “has estado en contacto con una
persona que dio COVID-19 positivo”, sin decir quién o dónde. **Si lo que
necesitamos es rastrear el patrón de movimiento del virus, no
necesitamos rastrear el de las personas**.

Se publicó [un artículo que compara ambos
protocolos](https://www.zfnd.org/blog/private-contact-tracing-protocols-compared/)
en un ‘esfuerzo por lograr que ambos protocolos se unan en un estándar
común’. Detalla las **características** de ambos sobre cómo responden
a las **cualidades deseables**: servidores respetuosos de la privacidad,
integridad de la fuente y la difusión \[enviar informes solo a los
dispositivos con los que se ha estado en contacto y no masivamente\], sin
seguimientos automáticos, privacidad del receptor y del emisor \[ninguna
de las dos partes tiene la posibilidad de revelar información diferente
a la que pudiera ser útil para combatir la pandemia\].

**En pocas semanas se avanzó veloz en protocolos de rastreo de contactos
descentralizados y con preservación de la privacidad**. Aún es necesario
atender a ciertos aspectos que garanticen esa privacidad en todos sus
tramos y sin embargo se hace esperanzador que el trabajo colaborativo dé
como resultado tangible la posibilidad de aplicaciones de rastreo seguro
que sirvan a los momentos actuales **sentando precedentes positivos
sobre la posibilidad de una tecnología de no-vigilancia**.

Este palabrerío está explicado de forma amena en una historieta
realizada por Nicky Case [pueden encontrar una versión del comic con
traducción al castellano rioplatense hecha por el Partido
Interdimensional Pirata por
aquí](https://utopia.partidopirata.com.ar/zines/la_vida_y_la_libertad.html).

Mientras tanto, **¿sería posible para los gobiernos latinoamericanos
adoptar estas tecnologías?** Conocimiento hay. Grupos de personas que
desarrollan tecnología localmente también. Y, como bien han demostrado
hasta ahora, ciertos recursos para invertir en ello, también. ¿Por qué
entonces no pensar que el dinero público puede ser invertido en grupos
locales y/o no-corporativos que sustentan valores democráticos al
intentar refrenar la pandemia?

En resumen, **es tecnológicamente posible desarrollar protocolos**
transparentes; proporcionales \[que obtengan datos centrados en el
seguimiento al virus, no a las personas\]; de código abierto, libre
y descentralizado; manteniendo la privacidad y anonimato de las personas
y que se desarrolle de manera transdisciplinar, colaborativa, para que
tome en consideración recabar solo los parámetros científicamente
necesarios durante la crisis sanitaria.

La política de los datos sí importa, determina la forma en que podremos
o no vivir en un espacio más libre en un futuro próximo. Esfuerzos
académicos, civiles y _hackers_ demuestran ‘con datos duros’ que
respetar nuestras libertades es posible si hay una decisión política de
fondo. ¿Gobiernos y corporaciones escucharán? Como quiera que sea, desde
**los espacios sociales tenemos a la mano argumentos sólidos que nos
permiten movernos del ‘no hay nada que podamos hacer’. Podemos, como
mínimo, informarnos sobre las opciones y exigir[nos] actuar en favor de
una tecnología que nos permita ampliar nuestras libertades colectivas**.

[_**@sursiendo**_](https://twitter.com/sursiendo)
