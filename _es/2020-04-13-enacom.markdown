---
title: 'En respuesta a las "buenas prácticas" del ENACOM'
description: |
  En estos días, desde la cuenta oficial del ENACOM se estuvieron
  difundiendo una lista de "buenas prácticas" para el uso de Internet
  con las que no coincidimos para nada.
author:
- Partido Interdimensional Pirata
categories:
- Argentina
layout: post
---

https://twitter.com/ENACOMArgentina/status/1241092934847664128?s=09

El mensaje nos cae para el culo. En el medio de un aislamiento social
reforzado con policía en la calle **comunicando por megáfonos que
tenemos que quedarnos encerrades**, se nos solicita un uso
**"responsable"** de la infraestructura de Internet.

Enacom es el ente regulador estatal dedicado a los servicios de
comunicación y con este tipo de mensajes se suma al discurso que desde
el gobierno refuerza la responsabilidad individual y poblacional
invisibilizando las políticas sistemáticas que han hecho posible tener
un servicio de salud pública colapsado como tb un mal funcionamiento de
internet. A esta situación de ofrecer una lista de las buenas prácticas,
un planteo con evidente tufillo punitivista, se sumó al poco tiempo un
mensaje no oficial, luego desmentido, difundiendo el pánico a que
internet se sature.

Pedir, de forma paternalista, que en un contexto de aislamiento social
reduzcamos el uso de una infraestructura clave para sostener algún tipo
de contacto afectivo, coordinar acciones de ayuda mutua o simplemente
entretenernos es, entonces, irresponsable de su parte.  No se trata solo
de estar tranquiles y bien informades, como buenes ciudadanes, sino de
poder seguir encontrándonos y cuidándonos pese al aislamiento. La
realidad a cerca del cuidado de nuestras comunidades no es como la
hegemonía la representa.  Internet, [con todas las críticas que le
hacemos](https://utopia.partidopirata.com.ar/zines/queremos-redes-libres.html),
es un medio que permite el encuentro.

Como viene sucediendo con muchas problemáticas que se agudizaron a raiz
del aislamiento obligatorio --el hacinamiento en las cárceles, los
abusos de las fuerzas represivas del estado, los efectos nocivos de los
agrotóxicos en la salud, la desatención al respecto de les infectades de
dengue, el oportunismo de las empresas que despiden empleades
masivamente y la explotación del necropoder patriarcal asesinando
mujeres cis y trans, travestis, personas trans y no binarias cada 23
horas--, el problema de la conectividad ya formaba parte de nuestra vida
cotidiana. Es decir, lo que está de fondo en el pedido del ENACOM es la
incapacidad de sostenerse que tiene el estado actual de la
infraestructura de red, y esto es debido a **la sobreventa de ancho de
banda que hacen los proveedores de Internet** donde, encima, les pobres
tenemos que abonar más por un servicio malo que no tiene la inversión
necesaría como para dar lo que ofrece, es evidente, por ejemplo, la
falta de inversión en tendido de fibra óptica. Como si fuera poco se le
suma que la gestión técnico-política de esta infraestructura funciona,
también al servicio de coroporaciones como Whatsapp y Mercadopago,
proporcionando conexiones gratuitas a estas empresas, vulnerando de esta
forma la neutralidad de la red.

> **Nuestro reclamo: ¡Internet libre y accesible sin restricciones para todes!**

Les piratas entendemos que el pedido del ENACOM de buenas prácticas se
aprovecha y abusa del nuevo sentido común que el aislamiento obligatorio
proyecta sobre las relaciones sociales e invisibiliza la organización
política y burocrática que atraviesa la gestión "técnica" de la
sociedad.

Al poner a les usuaries como objeto directo de esa gestión, se esconde
la dimensión logística del poder dando continuidad al paradigma clásico
de la política: quedan en las sombras las condiciones materiales de
existencia de la vida, desvinculándolas por lo tanto de lo político, lo
(re)productivo, lo doméstico, lo económico, así como de su importancia
para la supervivencia y la vida cotidiana.

Entonces, nos queda claro que, si nos piden buenas prácticas es para no
habilitar el debate en torno a las políticas que gestionan el
funcionamiento de Internet. Se prescribe un funcionamiento de Internet
por fuera de la agenda pública, planteando una forma silenciosa de
ejercer el poder.

> **La amenaza de la saturación funciona como una medida adoctrinante.**

Estamos viviendo en el mundo ideal del capitalismo cognitivo y el
control cibernético, donde desde nuestrxs dispositivxs generamos
plusvalor al mediar nuestras afectividades y creatividades a través de
plataformas dedicadas a su conversión y procesamiento en forma de
información.  Irónicamente, ¡ni siquiera tienen la infraestructura
preparada para sostenerlos!

## Lecturas relacionadas

[¡Queremos redes libres!](https://utopia.partidopirata.com.ar/zines/queremos-redes-libres.html)

[La revolución como problema técnico](https://utopia.partidopirata.com.ar/zines/de-curzio-malaparte-al-comite-invisible.html)

[La infraestructura no es un problema técnico](https://fauno.endefensadelsl.org/la-infraestructura-no-es-un-problema-tecnico/)
