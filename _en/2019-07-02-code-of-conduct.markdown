---
categories:
- Sutty
title: Code for sharing
author:
- Sutty
layout: post
permalink: code-of-conduct/
description: Codes of conduct allow inclusive communities
order: 0
---

# Code for sharing

> This code of conduct is based in "[Códigos para compartir, hackear,
> piratear en
> libertad](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html)"
> published by [Partido Interdimensional
> Pirata](https://partidopirata.com.ar/).

> We use gender neutral pronouns to include all peoples.  In this sense,
> we encourage different forms, strategies and tools used to embody
> practices that aren't anthropocentric, sexist, cis-sexist in our
> language.

## Introduction

This is an example code that strives to give a consensual frame to
enable asistance, permanence and confortable stay to everyone using and
inhabiting [Sutty](https://sutty.nl/), and to welcome new users and
potential allies as well.  It sets the floor for desirable and
acceptable, and undesirable and intolerable conducts for its community.
You can use it with or without changes, adapting it to your activities.
This code is in permanent and collective mutation and feeds, copies and
inspires on the following sources:

* <https://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy>

* <https://trans-code.org/code-of-conduct/>

* <https://openhardware.science/logistics/gosh-code-of-conduct/>

* <https://hypatiasoftware.org/code-of-conduct/>

We strive to sustain and foment an open community, that invites and
attains participation from more people, in all their diversity.  We know
that spaces related to computing and free software are mostly inhabited
by middle class cis white males, even when there's an acknowledgement of
the need to close the gender gap.  In this sense, this is our little
contribution, made from collective practices on multiple dimentions,
reflections, readings, and experiences that grow every day.

## That everyone needs to be well treated

Every being that we share space with deserves good treatment, respect
and compassion.  Here we share basic criteria for introduction and care.

### Towards humans

Everyone is deserving of care and greetings and we have a right to
assume good intentions from others.

When we refer to other humans, we try to be careful and respectul
towards their gender identity.  For this, these principles are useful:

* Don't assume, judge or try to "interpret" the gender of others.

* Don't use gender beforehand.  It's related to the previous item, but
  puts emphasis towards naturalized gendering behaviour (ie. assuming
  someone wearing a dress uses female pronouns...).  The proposal is to
  discard them.

* If a person explicits their pronouns and mode in which they want to be
  referenced, we respect them, by listening and trying to use their
  prefered pronouns.

* When presentations don't include pronouns, we can ask respectfully
  for prefered pronouns.  But be careful!  This question must be asked
  to everyone, otherwise the "suspicion" is loaded towards a person, and
  it can become a form of harrassment.

* Do we need to know the gender of a person to relate with them?  Maybe
  a better practice is to evade gendering others.  But if this means to
  use "them" compulsively, some people may be made to feel bad.  (For
  instance, trans\* people who use female or male pronouns may feel
  upset or outed when refering to them as "them", specially if they're
  the only ones to be gendered like this in a group!

* When in doubt, asking and apologizing respectfully is a good way of
  being careful towards each other.

## Important points to guarantee our space from being expulsive

**Listening to and between everyone in a caring climate**

* Listen to what everyone has to say, being mindful that everyone has
  something valuable to communicate.

* For active listening, we prefer to ask first, before making judgement.

* Sometimes being silent is a condition for others to be able to talk.
  To listen is an exercise that requires practice.  Also talking.

* We're interested in what everyone has to say.  If you're more trained
  in participating, talking, and having opinions, take into account that
  not everyone does.  Give them space if they want to take it.  But
  remember that encouraging is not the same as pressuring!

* We try to check and stop offensive practices to add to the respectful
  climate.  This doesn't mean to be submissive or to agree to
  everything.  At the least, it sets a floor of respect towards enabling
  a dialogue when necessary.

* It's at the very least disrepectful to repeat damaging behaviour when
  it was already identified as such.  It can make others unconfortable,
  or hurt and expel them.  We'll make this point every time that is
  needed and tolerable.

* We avoid this behaviour ourselves and we help others to notice their
  own.

* When raising attention becomes insufficient, we need to review this
  agreements to keep the coexistence.  This implies to act in accord to
  them, and that this code can be revised and updated when deemed
  necessary (there's no consensus).

* One of the ways in which free software spaces can be and are expulsive
  is with attitudes that don't contemplate diversity in knowledges and
  interlocutors.  By appealing to technicisms, many comrades are kept
  out of what's happening, and no one verifies if everyone is keeping up
  with the conversation.

  Our fervent recommendation is to be attentive to this dynamic so we
  can avoid or revert them.

* The counter to the previous situation is "mansplaining": a cis-male
  person assuming the authoritative place of knowledge to (over-)explain
  everything to others, in a patronizing way and without taking into
  account what others want to listen or not, to say, what already know
  or do, etc.

* We believe there's no "authoritative voice" to have an opinion and to
  participate.  Free culture is for everyone to share.

* "Sharing is caring" v. "Google is your friend".  Meritocracy and other
  traditional codes in cyber-communities work against free culture.  We
  support the pirate culture that onboards ever more pirates to their
  ships.  We believe that culture is for everyone and we defy elitisms.

* We don't assume that other people shares our likings, beliefs, class
  position, sexuality, etc.  We can be violent when we misread others.
  We recommend to ask respectfully and to avoid comments or jokes that
  can be hurtful to others.

* We speak and act gently and inclusively.

* We respect different points of view, experiences, beliefs, etc. and we
  take them into account when we act collectively so it reflects in our
  attitudes.

* We welcome criticism, specially the constructive kind ;)

* We focus in what's best for the community, without losing warmth,
  respect and diversity amongst ourselves.

* We show empathy toward others.  We want to share and communicate.

* It's useful to think everyone has different abilities, stories,
  experiences... It's possible to not understand some comments.  We try
  to avoid acting in bad faith and to use every accessibility tool we
  can.

* The last item includes neurodiverse people and those that have
  experienced trauma.  Sometimes, sarcasm or irony is not well received
  or understood by others.  We take this into our strategies to include
  everyone in our communications.  Even more, if we think some topics
  could be sensitive to others (memory-triggering, phobias, untolerable,
  explicit violence or body images, etc.), we use content warnings (cw)
  before what we wanted to share.  For instance: "cw: comments about
  sexual and physical violence".  This allows everyone to opt in to the
  content instead of being taken by surprise.

* We're respectful of limits established by others (personal space,
  physical contact, interaction mood, privacy, being photographed, etc.)

* We want to and believe in welcoming more pirates!

## Consent for documenting and sharing in media

* If you're going to take pictures or record video, ask consent from
  people involved.

* If there're minors, ask their responsible families.

## Our commitment against harassment

In the interest of fomenting an open, diverse and welcoming community,
we contributors and admins make a commitment against harassment in our
projects and community for everyone, without regard of age, body
diversity, capacity, neuro-diversity, ethnicity, gender identity and
expression, experience level, nationality, physical appearance,
religion, sexual identity or orientation.

Examples of unacceptable behaviour from participants:

* Offensive comments about gender/s, gender identity and expression,
  sexual orientation, capacity, mental sickness, neuro-(a)tipicality,
  physical appearance, body size, ethnicity or religion.

* Unwelcomed comments related to personal and life choices, including
  amongst others, those related to food, health, children upbringing,
  drug use and employment.

* Insulting or despective comments and personal or political attacks.
  **Trolling**.

* Assuming others' gender.  If you're in doubt, ask politely about
  pronouns.  Don't use the name(s) that people don't use anymore, use
  the name, _nickname_ or pseudonym that they prefer.  Do you really
  need the name, ID number, biometric data, birth certificate of others?

* Sexual comments, images or behaviour, unneeded or in spaces where they
  weren't appropiate.

* Unconsented physical contact or repeated after being asked to stop.

* Threatening others.

* Inciting violence towards others, including self-damage.

* Deliberate intimidation.

* Stalking.

* To harass by photographing or recording without consent, including
  uploading personal information to the Internet.

* Interrupting a conversation constantly.

* Making unwanted sexual comments.

* Unappropiate patterns of social contact, like asking/assuming
  inappropiate intimacy levels with others.

* Trying to interact with a person after being asked not to.

* Exposing deliberately any aspect of a person identity without consent,
  except when necessary for protecting others against intentional abuse.

* Making public any kind of private conversation.

* Other kinds of conduct that can be considered inappropiate in an
  environment of camaraderie.

* Repeating attitudes that others find offensive or violatory of this
  code.

## Consequences

* Any person that has been asked to stop offensive behaviour is expected
  to respond immediately, even when in disagreement.

* Admins can take any action deemed necessary and adequate, including
  expelling the person or removing their site without advertence.  This
  decision is taken by consensus between admins and is reserved for
  extreme cases that compromise the community or the permanence of
  others without feeling wronged or threatened.

* Admins reserve the right to forbid participation to any future
  activity or publication.

As we mentioned before, this code is in permanent collective mutation.
It's main objective is to generate an inclusive and non-expulsive
environment that is also transparent and open without [missing
stairs](https://en.wikipedia.org/wiki/Missing_stair) ("the missing stair
from a house that everyone knows about but no one wants to take
responsibility for").  It's important to adapt it to different
activities and that it nurtures from contributions from its users.
Receiving your comments and input will help us to achieve this
objective.

## Let's keep in contact!

Si pasaste por alguna situación que quieras compartir --te hayas animado
o no a decirlo en el momento--, podés ponerte en contacto con nosotres.

Con respecto a quejas o avisos acerca de situaciones de violencia,
acoso, abuso o repetición de conductas que se advirtieron como
intolerables, tomamos la responsabilidad de tenerlas en cuenta y
trabajar en ellas para que el resultado sea el favorable al espíritu de
colectiva que elegimos y describimos aquí.  Si bien consideramos que las
prácticas punitivistas no van con nosotres, nuestra decisión explícita
es escuchar a la persona que se manifiesta como violentada o víctima y
acompañarla.

You can contact us if you were part of a situation you want to share
--even if you didn't pointed it in the moment.

In regards to complaints or notices about violence, harassment, abuse or
repeated untolerable conducts, we take the responsibility of working on
them for a favorable result towards the collective spirit we defined
here.  Even when we don't condone punitivist practices, our explicit
decision is for the victim to be listened and accompanied.
